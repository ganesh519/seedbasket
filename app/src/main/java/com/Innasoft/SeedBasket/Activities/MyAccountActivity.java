package com.Innasoft.SeedBasket.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;

import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Singleton.AppController;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyAccountActivity extends AppCompatActivity {


    @BindView(R.id.txtAccName)
    TextView txtAccName;
    @BindView(R.id.txtAccMobile)
    TextView txtAccMobile;
    @BindView(R.id.txtMail)
    TextView txtMail;
    @BindView(R.id.ChangePassword)
    CardView ChangePassword;
    @BindView(R.id.myAddressCard)
    CardView myAddressCard;
    @BindView(R.id.myOrdersCard)
    CardView myOrdersCard;
    @BindView(R.id.myWhishListCard)
    CardView myWhishListCard;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.parentLayout)
    NestedScrollView parentLayout;
    @BindView(R.id.editProfile)
    ImageView editProfile;
    @BindView(R.id.myNotificationsCard)
    CardView myNotificationsCard;
    @BindView(R.id.image)
    ImageView image;

    private PrefManagerUser pref;
    String userId;
    boolean checkoutStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Account");


        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);

        }


        AppController app = (AppController) getApplication();
        pref = new PrefManagerUser(getApplicationContext());

        // Method to manually check connection status

        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        String username = profile.get("name");
        String mobile = profile.get("mobile");
        String email = profile.get("email");
        String imagePic = profile.get("profilepic");


        txtAccName.setText(username);
        txtAccMobile.setText(mobile);
        txtMail.setText(email);
        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {
            Picasso.with(getApplicationContext()).load(imagePic).error(R.drawable.ic_user).into(image);

        }
        else {

            image.setImageResource(R.drawable.ic_user);
        }



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick({R.id.ChangePassword, R.id.myAddressCard, R.id.myOrdersCard, R.id.myWhishListCard, R.id.myNotificationsCard, R.id.editProfile,R.id.mywishlist})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ChangePassword:
//                Intent editMyAcc = new Intent(MyAccountActivity.this, ChangePasswordActivity.class);
//                editMyAcc.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(editMyAcc);

                break;
            case R.id.myAddressCard:
                Intent address = new Intent(MyAccountActivity.this, AddressListActivity.class);
                address.putExtra("Checkout", false);
                address.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(address);

                break;
            case R.id.myOrdersCard:
                Intent orders = new Intent(MyAccountActivity.this, OrderIdActvity.class);
                orders.putExtra("Checkout", false);
                orders.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(orders);

                break;
            case R.id.mywishlist:
                Intent wishlist = new Intent(MyAccountActivity.this, WishListActivity.class);
                wishlist.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(wishlist);

                break;
            case R.id.editProfile:
                Intent vechils = new Intent(MyAccountActivity.this, UpdateProfileActivity.class);
                vechils.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(vechils);

                break;
            case R.id.myNotificationsCard:
//                Intent service = new Intent(MyAccountActivity.this, NotificationActivity.class);
//                service.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(service);

                break;
        }
    }
}
