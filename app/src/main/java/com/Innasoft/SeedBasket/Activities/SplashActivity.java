package com.Innasoft.SeedBasket.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


import com.Innasoft.SeedBasket.R;

public class SplashActivity extends Activity {
    public static String USER = "Users";
    public static String ADMIN = "Admin";
    private static  int SPLASH_TIME_OUT = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent  = new Intent(SplashActivity.this, LoginActivity.class);
                intent.putExtra("ROLE",USER);
                intent.putExtra("signup", "Splash");
                startActivity(intent);

            }
        }, SPLASH_TIME_OUT);

    }
}
