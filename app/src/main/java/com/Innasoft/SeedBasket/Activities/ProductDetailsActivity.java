package com.Innasoft.SeedBasket.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.Innasoft.SeedBasket.Utilities.Constants;
import com.Innasoft.SeedBasket.Model.ProductsModel;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Singleton.AppController;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;
import com.Innasoft.SeedBasket.Model.CartorWistStatus;
import com.Innasoft.SeedBasket.Model.Product;
import com.Innasoft.SeedBasket.Utilities.Converter;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;

import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_CART;
import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_WISHLIST;

public class ProductDetailsActivity extends AppCompatActivity {

    @BindView(R.id.banner_slider1)
    BannerSlider bannerSlider1;
    @BindView(R.id.txtProductName)
    TextView txtProductName;
    @BindView(R.id.txtWeight)
    TextView txtWeight;
    @BindView(R.id.txtDisPrice)
    TextView txtDisPrice;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.product_minus)
    TextView productMinus;
    @BindView(R.id.product_quantity)
    TextView productQuantity;
    @BindView(R.id.product_plus)
    TextView productPlus;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.btnAddtocart1)
    Button btnAddtocart;
    @BindView(R.id.bootamLayout)
    CardView bootamLayout;
    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;
    @BindView(R.id.btnAddtoFavorite)
    Button btnAddtoFavorite;

    //firebase objects
    private DatabaseReference mDatabase;
    private String formattedDate;
    StorageReference sRef;
    private String TAG = "ProductDetailsActivity";
    int count = 1;


    private FirebaseStorage mStorage;

    //list to hold all the uploaded images
    private List<ProductsModel> productsModelList;
    private List<CartorWistStatus> cartorWistStatusList;

    String catMKey, subCatMKey, productName, mKey, userId, id, currentDate, imageType, imageUrl,
            category, subcategory, productPrice, capacityWeight, availability, description, wishlistStatus, cartListStatus;
    private PrefManagerUser pref;
    int cartindex = 0;
    AppController appController;
    MenuItem menuItem1, menuItem2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);

        appController = (AppController) getApplication();
        pref = new PrefManagerUser(getApplicationContext());
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");

        if (getIntent() != null) {
            catMKey = getIntent().getStringExtra("CatMKey");
            subCatMKey = getIntent().getStringExtra("subcatMkey");
            productName = getIntent().getStringExtra("productName");
            mKey = getIntent().getStringExtra("mKey");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(productName);


        productsData(catMKey, subCatMKey, mKey);


        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());


    }

    private void productsData(String catMKey, String subCatmKey, String mKey) {

        progressBar2.setVisibility(View.VISIBLE);
        productsModelList = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_PRODUCTS).child(catMKey).child(subCatmKey).child(mKey);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog

                progressBar2.setVisibility(View.GONE);
                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    productsModelList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {


                    }

                    ProductsModel upload = snapshot.getValue(ProductsModel.class);
                    upload.setmKey(snapshot.getKey());
                    productsModelList.add(upload);

                    for (ProductsModel productsModel : productsModelList) {

                        if (productsModel.getmKey().equalsIgnoreCase(mKey)) {

                            id = productsModel.getId();
                            currentDate = productsModel.getCurrentDate();
                            imageType = productsModel.getImageType();
                            imageUrl = productsModel.getImageUrl();
                            category = productsModel.getCategory();
                            subcategory = productsModel.getSubcategory();
                            productPrice = productsModel.getProductPrice();
                            capacityWeight = productsModel.getCapacityWeight();
                            availability = productsModel.getAvailability();
                            description = productsModel.getDescription();

                            // home banners
                            List<Banner> banners = new ArrayList<>();
                            BannerSlider bannerSlider = (BannerSlider) findViewById(R.id.banner_slider1);
                            banners.add(new RemoteBanner(productsModel.imageUrl));

                            //add banner using resource drawable
                            bannerSlider.setBanners(banners);
                            bannerSlider.onIndicatorSizeChange();

                            txtProductName.setText(productsModel.productName);
                            txtPrice.setText(getResources().getString(R.string.Rs) + productsModel.productPrice);
                            txtWeight.setText(productsModel.capacityWeight);
                            txtDescription.setText(productsModel.description);

                            productMinus.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (count > 1) {
                                        count--;
                                        productQuantity.setText("" + count);
                                    }
                                }
                            });

                            // quantity increase
                            productPlus.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    count++;
                                    productQuantity.setText("" + count);
                                }
                            });


                        }

                    }

                    status(catMKey, subCatMKey, mKey, id);

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar2.setVisibility(View.GONE);


            }
        });


    }

    private void status(String catMKey, String subCatmKey, String mKey, String id) {

        progressBar2.setVisibility(View.VISIBLE);
        cartorWistStatusList = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.CART_WISH_STATUS).child(catMKey).child(subCatmKey).child(mKey);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog

                progressBar2.setVisibility(View.GONE);
                Log.e("STATUSNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    cartorWistStatusList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        CartorWistStatus upload = postSnapshot.getValue(CartorWistStatus.class);
                        upload.setmKey(userId);
                        cartorWistStatusList.add(upload);

                    }


                    for (CartorWistStatus cartorWistStatus : cartorWistStatusList) {

                        if (cartorWistStatus.getUserId().equalsIgnoreCase(userId)) {

                            wishlistStatus = cartorWistStatus.getWishlistStatus();
                            cartListStatus = cartorWistStatus.getCartStatus();

                            // wishlist status
                            if (wishlistStatus.equalsIgnoreCase("1")) {
                                btnAddtoFavorite.setText("Go to Favorites");
                            } else {
                                btnAddtoFavorite.setText("Add to Favorites");
                            }

                            // cart status
                            if (cartorWistStatus.getCartStatus().equalsIgnoreCase("1")) {
                                btnAddtocart.setText("Go to Cart");

                            } else {
                                btnAddtocart.setText("Add to Cart");

                            }
                        }
                    }


                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar2.setVisibility(View.GONE);


            }
        });


    }


    @OnClick({R.id.btnAddtocart1,R.id.btnAddtoFavorite})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnAddtocart1:
                if (btnAddtocart.getText().equals("Go to Cart")) {
                    Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    addtocart();
                }
                break;
            case R.id.btnAddtoFavorite:
                if (btnAddtoFavorite.getText().equals("Go to Favorites")) {
                    Intent intent = new Intent(getApplicationContext(), WishListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    whishlist();
                }
                break;


        }
    }

    private void whishlist() {
        btnAddtoFavorite.setText("Go to Favorites");

            Product product = new Product();
            //Adding values
            product.setUserId(userId);
            product.setId(id);
            product.setCurrentDate(currentDate);
            product.setImageType(imageType);
            product.setImageUrl(imageUrl);
            product.setCategory(category);
            product.setSubcategory(subcategory);
            product.setProductName(productName);
            product.setProductPrice(Double.parseDouble(productPrice));
            product.setCapacityWeight(capacityWeight);
            product.setAvailability(availability);
            product.setDescription(description);
            product.setmKey(mKey);
            product.setQuantity(1);
            product.setCatMKey(catMKey);
            product.setSubCatMKey(subCatMKey);
            if (null != cartListStatus) {
                product.setCartStatus(cartListStatus);
            } else {
                product.setCartStatus("0");
            }
            product.setWishStatus("1");


            mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_WISHLIST).child(userId);
            // String uploadId = mDatabase.push().getKey();
            DatabaseReference newRef = mDatabase.push();
            newRef.setValue(product);


            if (null != cartListStatus) {
                updateCart(userId, cartListStatus, "1", id);
            } else {
                updateCart(userId, "0", "1", id);

            }

    }

    private void addtocart() {

        String p_qnut=productQuantity.getText().toString().trim();
        int value=Integer.valueOf(p_qnut);
        btnAddtocart.setText("Go To Cart");

        Product product = new Product();
        //Adding values
        product.setUserId(userId);
        product.setId(id);
        product.setCurrentDate(currentDate);
        product.setImageType(imageType);
        product.setImageUrl(imageUrl);
        product.setCategory(category);
        product.setSubcategory(subcategory);
        product.setProductName(productName);
        product.setProductPrice(Double.parseDouble(productPrice));
        product.setCapacityWeight(capacityWeight);
        product.setAvailability(availability);
        product.setDescription(description);
        product.setmKey(mKey);
        product.setQuantity(value);
        product.setCatMKey(catMKey);
        product.setSubCatMKey(subCatMKey);
        product.setCartStatus("1");

        if (null != wishlistStatus) {
            product.setWishStatus(wishlistStatus);
        } else {
            product.setWishStatus("0");

        }


        mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_CART).child(userId);
        // String uploadId = mDatabase.push().getKey();
        DatabaseReference newRef = mDatabase.push();
        newRef.setValue(product);

        if (null != cartListStatus) {
            updateCart(userId, "1", wishlistStatus, id);

        } else {
            updateCart(userId, "1", "0", id);

        }

        cartindex = cartindex + 1;
        Log.e("CARTPLUSCOUNT", "" + cartindex);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("itemCount", cartindex);
        editor.apply();
        invalidateOptionsMenu();


    }


    private void updateCart(String userId, String cartStatus, String wishStatus, String id) {
        //getting the specified artist reference
        // mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(catMKey).child(Constants.DATABASE_PATH_SUBCATEGORY).child(subCatMKey).child(Constants.DATABASE_PATH_PRODUCTS).child(id).child(CART_WISH_STATUS);
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.CART_WISH_STATUS).child(catMKey).child(subCatMKey).child(id);

        //updating artist
        CartorWistStatus product = new CartorWistStatus();
        //Adding values
        product.setUserId(userId);
        product.setCartStatus(cartStatus);
        product.setWishlistStatus(wishStatus);
        // CartorWistStatus product = new CartorWistStatus(userId, cartStatus, wishStatus);
        mDatabase.child(userId).setValue(product);

    }


    @Override
    protected void onRestart() {

        appController.cartCount(userId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onRestart();
    }

    @Override
    protected void onStart() {
        appController.cartCount(userId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onStart();
    }

    ///
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.options_menu, menu);
        final MenuItem menuItemCart = menu.findItem(R.id.action_cart);
        menuItemCart.setIcon(Converter.convertLayoutToImage(ProductDetailsActivity.this, cartindex, R.drawable.ic_actionbar_bag));

        /*final MenuItem alertMenuItem = menu.findItem(R.id.favorite);
        LinearLayout rootView = (LinearLayout) alertMenuItem.getActionView();
        materialFavoriteButton = (MaterialFavoriteButton) rootView.findViewById(R.id.favorite_nice);
        materialFavoriteButton.setOnFavoriteChangeListener(
                (buttonView, favorite) ->

                        // Snackbar.make(buttonView, getString(R.string.toolbar_favorite_snack) + favorite, Snackbar.LENGTH_SHORT).show()
                        whishlist()
        );


        rootView.setOnClickListener(v -> onOptionsItemSelected(alertMenuItem));
*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_cart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

           /* case R.id.favorite:

                break;
*/

        }
        return super.onOptionsItemSelected(item);
    }


}
