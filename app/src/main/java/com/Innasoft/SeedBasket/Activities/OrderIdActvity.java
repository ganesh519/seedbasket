package com.Innasoft.SeedBasket.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.Innasoft.SeedBasket.Adapters.OrderIdAdapter;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Singleton.AppController;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderIdActvity extends AppCompatActivity {
    int color = Color.RED;
    AppController appController;
    @BindView(R.id.orderidRecycler)
    RecyclerView orderidRecycler;
    @BindView(R.id.txtError)
    TextView txtError;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    PrefManagerUser pref;

    OrderIdAdapter myOrderAdapter;
//    List<OrderIdItem> orderIdItemList = new ArrayList<>();
    String UserID, orderId;
    String userId,tokenValue,deviceId;
    boolean checkoutStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_id_actvity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Orders");
        appController = (AppController) getApplication();

        pref = new PrefManagerUser(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");

        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);

        }


        if (appController.isConnection()) {

            prepareOrderIdData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });



        }
    }


    private void prepareOrderIdData() {

        progressBar.setVisibility(View.VISIBLE);

//        Call<MyOrdersResponse> call=RetrofitClient.getInstance().getApi().MyOrders(tokenValue,userId);
//        call.enqueue(new Callback<MyOrdersResponse>() {
//            @Override
//            public void onResponse(Call<MyOrdersResponse> call, Response<MyOrdersResponse> response) {
//                if (response.isSuccessful());
//                MyOrdersResponse myOrdersResponse=response.body();
//
//                if (myOrdersResponse.getStatus().equals("10100")){
//
//                    progressBar.setVisibility(View.GONE);
//
////                    Toast.makeText(OrderIdActvity.this, myOrdersResponse.getMessage(), Toast.LENGTH_SHORT).show();
//
//                    List<MyOrdersResponse.DataBean.RecordDataBean> modulesBeanList = response.body().getData().getRecordData();
//                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
//                    orderidRecycler.setLayoutManager(layoutManager);
//                    orderidRecycler.setItemAnimator(new DefaultItemAnimator());
//
//                    myOrderAdapter = new OrderIdAdapter(getApplicationContext(),modulesBeanList);
//                    orderidRecycler.setAdapter(myOrderAdapter);
//                    myOrderAdapter.notifyDataSetChanged();
//
//                }
//                else if (myOrdersResponse.getStatus().equals("10200")){
//                    progressBar.setVisibility(View.GONE);
//                    Toast.makeText(OrderIdActvity.this, myOrdersResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//                else if (myOrdersResponse.getStatus().equals("10300")){
//                    progressBar.setVisibility(View.GONE);
//                    Toast.makeText(OrderIdActvity.this, myOrdersResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                    txtError.setVisibility(View.VISIBLE);
//                    txtError.setText("No Orders Found !");
//
//                }
//                else if (myOrdersResponse.getStatus().equals("10400")){
//                    progressBar.setVisibility(View.GONE);
//                    Toast.makeText(OrderIdActvity.this, myOrdersResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//                else {
//                    txtError.setVisibility(View.VISIBLE);
//                    txtError.setText("No Orders Found !");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<MyOrdersResponse> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
//                Toast.makeText(OrderIdActvity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });



    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;


        }

        return super.onOptionsItemSelected(item);
    }
}
