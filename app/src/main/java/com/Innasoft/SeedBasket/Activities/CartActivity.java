package com.Innasoft.SeedBasket.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.Innasoft.SeedBasket.Model.AddAddressModel;
import com.Innasoft.SeedBasket.Utilities.Constants;
import com.Innasoft.SeedBasket.Interface.Cartinterface;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Singleton.AppController;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;
import com.Innasoft.SeedBasket.Adapters.CartAdapter;
import com.Innasoft.SeedBasket.Model.CartorWistStatus;
import com.Innasoft.SeedBasket.Model.Product;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_CART;

public class CartActivity extends AppCompatActivity implements Cartinterface {

    private final String TAG ="CartActivity" ;
    @BindView(R.id.txtAlert)
    TextView txtAlert;
    @BindView(R.id.txtSubTotal)
    TextView txtSubTotal;
    @BindView(R.id.btnCheckOut)
    Button btnCheckOut;
    @BindView(R.id.bottamlayout)
    RelativeLayout bottamlayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.simpleSwipeRefreshLayout)
    RelativeLayout simpleSwipeRefreshLayout;
    private DatabaseReference mDatabase;
    @BindView(R.id.recyclerCart)
    RecyclerView recyclerCart;
    ArrayList<Product> productArrayList;
    ArrayList<AddAddressModel> addressModelArrayList;

    Cartinterface cartinterface;
    CartAdapter cartAdapter;
    AppController appController;
    private PrefManagerUser pref;
    String userId,addressId;
    int minimumOrder = 300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cart");

        pref = new PrefManagerUser(getApplicationContext());
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");

        appController = (AppController) getApplication();
        cartinterface = (Cartinterface) this;

        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerCart.setLayoutManager(mLayoutManager1);
        recyclerCart.setItemAnimator(new DefaultItemAnimator());

        prepareCartData();
        appController.cartCount(userId);
        addressList();

    }


    private void prepareCartData() {
        progressBar.setVisibility(View.VISIBLE);

        productArrayList = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_CART).child(userId);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                progressBar.setVisibility(View.GONE);


                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    productArrayList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Product upload = postSnapshot.getValue(Product.class);
                        upload.setmKey(postSnapshot.getKey());
                        productArrayList.add(upload);

                    }
                    cartAdapter = new CartAdapter(CartActivity.this, productArrayList, cartinterface, "", "");
                    recyclerCart.setAdapter(cartAdapter);
                    cartAdapter.notifyDataSetChanged();


                }

                calculateCartTotal();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);            }
        });
    }


    @Override
    public void onMinusClick(Product product) {
        int i = productArrayList.indexOf(product);

        if (product.quantity > 1) {

            Product updatedProduct = new Product(
                    product.userId,
                    product.id,
                    product.currentDate,
                    product.imageType,
                    product.imageUrl,
                    product.category,
                    product.subcategory,
                    product.productName,
                    product.productPrice,
                    product.capacityWeight,
                    product.availability,
                    product.description,
                    product.mKey,
                    product.quantity - 1,
                    product.catMKey,
                    product.subCatMKey,
                    product.cartStatus,
                    product.wishStatus


            );
            productArrayList.remove(product);
            productArrayList.add(i, updatedProduct);
            cartAdapter.notifyDataSetChanged();

            calculateCartTotal();
            updateQuantity(updatedProduct);

        }
    }

    @Override
    public void onPlusClick(Product product) {
        int i = productArrayList.indexOf(product);

        Product updatedProduct = new Product(
                product.userId,
                product.id,
                product.currentDate,
                product.imageType,
                product.imageUrl,
                product.category,
                product.subcategory,
                product.productName,
                product.productPrice,
                product.capacityWeight,
                product.availability,
                product.description,
                product.mKey,
                product.quantity + 1,
                product.catMKey,
                product.subCatMKey,
                product.cartStatus,
                product.wishStatus

        );
        productArrayList.remove(product);
        productArrayList.add(i, updatedProduct);
        cartAdapter.notifyDataSetChanged();

        calculateCartTotal();
        updateQuantity(updatedProduct);

    }


    @Override
    public void onRemoveDialog(Product product) {

        deleteCartItem(product.getmKey(), product);
        updateCart(userId, product);


    }

    @Override
    public void onWishListDialog(Product product) {

    }


    // delete cart item
    private boolean deleteCartItem(String mKey, Product product) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_CART).child(userId).child(mKey);
        //removing artist
        dR.removeValue();
        productArrayList.remove(product);
        calculateCartTotal();
        cartAdapter.notifyDataSetChanged();

        // Toast.makeText(getApplicationContext(), "Post Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    // update quantity
    private void updateQuantity(Product updatedProduct) {

        Log.d(TAG, "updateQuantity: "+updatedProduct.getmKey());
        //getting the specified artist reference
        mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_CART).child(userId).child(updatedProduct.mKey);
        mDatabase.setValue(updatedProduct);


    }

   // update cart status
    private void updateCart(String userId, Product product) {

        Log.d(TAG, "updateCart: "+product.getmKey()+"---"+product.getSubCatMKey()+"---"+product.getCatMKey());
        //getting the specified artist reference
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.CART_WISH_STATUS).child(product.catMKey).child(product.subCatMKey).child(product.id);

        //updating artist
        CartorWistStatus cartorWistStatus = new CartorWistStatus();
        //Adding values
        cartorWistStatus.setUserId(userId);
        cartorWistStatus.setCartStatus("0");
        cartorWistStatus.setWishlistStatus(product.wishStatus);
        // CartorWistStatus product = new CartorWistStatus(userId, cartStatus, wishStatus);
        mDatabase.child(userId).setValue(cartorWistStatus);


    }


    private void addressList(){
        progressBar.setVisibility(View.VISIBLE);
        addressModelArrayList = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_ADDRESS).child(userId);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog

                progressBar.setVisibility(View.GONE);
                Log.e("STATUSNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    addressModelArrayList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        AddAddressModel upload = postSnapshot.getValue(AddAddressModel.class);
                        //upload.setKey(userId);
                        addressModelArrayList.add(upload);

                    }

                    for (AddAddressModel addAddressModel : addressModelArrayList){

                        Log.d(TAG, "onDataChange: "+addAddressModel.getCheck());
                        if (addAddressModel.getCheck().equalsIgnoreCase("Yes")){

                            Log.d(TAG, "onDataChange1: "+addAddressModel.getKey());
                            addressId = addAddressModel.getKey();
                        }

                    }


                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);


            }
        });
    }


    // total Amount
    public void calculateCartTotal() {

        int grandTotal = 0;

        for (Product order : productArrayList) {

            grandTotal += order.getProductPrice() * order.getQuantity();

        }

        Log.e("TOTAL", "" + productArrayList.size() + "Items | " + "  DISCOUNT : " + grandTotal);

        txtSubTotal.setText(productArrayList.size() + " Items | Rs " + grandTotal);

        if (grandTotal >= minimumOrder) {

            btnCheckOut.setVisibility(View.VISIBLE);
            txtAlert.setVisibility(View.GONE);
        } else {

            btnCheckOut.setVisibility(View.INVISIBLE);
            txtAlert.setVisibility(View.VISIBLE);
            txtAlert.setText("Minimum Order Must Be Greater Than Rs." + minimumOrder + "*");

        }

        int finalGrandTotal = grandTotal;
        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CartActivity.this, CheckoutActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Checkout", true);
                intent.putExtra("subTotal", finalGrandTotal);
                intent.putExtra("addressId", addressId);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        if (productArrayList.size() == 0) {

            SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("itemCount", 0);
            editor.apply();
            invalidateOptionsMenu();

            setContentView(R.layout.emptycart);
            Button btnGotohome = (Button) findViewById(R.id.btnGotohome);
            btnGotohome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(CartActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }



    }


    private double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }


    @Override
    protected void onRestart() {

       /* appController.cartCount(userid);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);*/
        invalidateOptionsMenu();
        super.onRestart();
    }

    @Override
    protected void onStart() {
       /* appController.cartCount(userid);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);*/
        invalidateOptionsMenu();
        super.onStart();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;


        }
        return super.onOptionsItemSelected(item);
    }

}
