package com.Innasoft.SeedBasket.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.Innasoft.SeedBasket.Fragment.LoginFragment;
import com.Innasoft.SeedBasket.Fragment.RegisterFragment;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    TabLayout tabLayout;
    public static ViewPager viewPager;
    ViewPagerAdapterNotifications viewPagerAdapter;
    String Signup;
    PrefManagerUser prefManagerUser;
    boolean doubleBackToExitPressedOnce = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_activity);


        prefManagerUser = new PrefManagerUser(this);
        if (prefManagerUser.isLoggedIn()) {
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            finish();
        }

        String role=getIntent().getStringExtra("ROLE");

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Role",role);
        editor.apply();

        viewPager = (ViewPager) findViewById(R.id.viewPager_login);
        viewPagerAdapter = new ViewPagerAdapterNotifications(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new LoginFragment(), "Login");
        viewPagerAdapter.addFragment(new RegisterFragment(), "Signup");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs_login);
        tabLayout.setupWithViewPager(viewPager);


        Signup = getIntent().getStringExtra("signup");

        if (!Signup.equals("")) {
            if (Signup.equals("Sign")) {

                viewPager.setCurrentItem(1);
            } else {

                viewPager.setCurrentItem(0);
            }
        } else {

            viewPager.setCurrentItem(0);
        }
    }

    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapterNotifications(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            moveTaskToBack(true);
            Process.killProcess(Process.myPid());
            System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
