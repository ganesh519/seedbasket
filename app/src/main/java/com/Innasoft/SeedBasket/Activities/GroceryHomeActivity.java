package com.Innasoft.SeedBasket.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.Innasoft.SeedBasket.Utilities.Constants;
import com.Innasoft.SeedBasket.Model.ProductsModel;
import com.Innasoft.SeedBasket.Model.SubCategoryModel;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Singleton.AppController;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;
import com.Innasoft.SeedBasket.Adapters.ProductSectionListDataAdapter;
import com.Innasoft.SeedBasket.Adapters.SubCatRecyclerViewDataAdapter;
import com.Innasoft.SeedBasket.Adapters.SubCatageoryAdapter;
import com.Innasoft.SeedBasket.Model.HeaderSectionDataModel;
import com.Innasoft.SeedBasket.Model.SingleItemModel;
import com.Innasoft.SeedBasket.Utilities.Converter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroceryHomeActivity extends AppCompatActivity {

    @BindView(R.id.catRecycler)
    RecyclerView catRecycler;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.simpleSwipeRefreshLayout)
    SwipeRefreshLayout simpleSwipeRefreshLayout;


    ArrayList<HeaderSectionDataModel> allSampleData;
    @BindView(R.id.txtError)
    TextView txtError;


    private SubCatageoryAdapter adapter;
    private SubCatRecyclerViewDataAdapter subCatRecyclerViewDataAdapter;
    private ProductSectionListDataAdapter productSectionListDataAdapter;

    //database reference
    private DatabaseReference mDatabase;

    private List<SubCategoryModel> subCategoryModelList;
    private List<ProductsModel> productsModelList;

    String CatMKey, categoryname;
    String userId;
    int cartindex;

    private PrefManagerUser pref;
    AppController appController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_home);
        ButterKnife.bind(this);

        pref = new PrefManagerUser(this);
        appController = (AppController) getApplication();

        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        String username = profile.get("name");
        String email = profile.get("email");


        if (getIntent() != null) {
            CatMKey = getIntent().getStringExtra("MKey");
            categoryname = getIntent().getStringExtra("CatName");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(categoryname);


        allSampleData = new ArrayList<HeaderSectionDataModel>();

        simpleSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorBlue, R.color.colorPrimary);

        simpleSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                prepareSubCatData();

                simpleSwipeRefreshLayout.setRefreshing(false);


            }
        });

        prepareSubCatData();

        catRecycler.setHasFixedSize(true);
        catRecycler.setLayoutManager(new LinearLayoutManager(GroceryHomeActivity.this, RecyclerView.HORIZONTAL, false));
        catRecycler.setItemAnimator(new DefaultItemAnimator());


    }

    private void prepareSubCatData() {

        simpleSwipeRefreshLayout.setRefreshing(true);
        subCategoryModelList = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_SUBCATEGORY).child(CatMKey);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                simpleSwipeRefreshLayout.setRefreshing(false);

                Log.e("SNAPSHOTSubCatData", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    subCategoryModelList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        SubCategoryModel upload = postSnapshot.getValue(SubCategoryModel.class);
                        upload.setmKey(postSnapshot.getKey());
                        subCategoryModelList.add(upload);

                    }

                    // subCat Recycler
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    catRecycler.setLayoutManager(mLayoutManager);
                    catRecycler.setItemAnimator(new DefaultItemAnimator());
                    catRecycler.setHasFixedSize(true);
                    catRecycler.setNestedScrollingEnabled(false);

                    // subcat Adapter
                    adapter = new SubCatageoryAdapter(getApplicationContext(), subCategoryModelList, CatMKey);
                    catRecycler.setAdapter(adapter);
                    adapter.notifyDataSetChanged();


                    allSampleData.clear();
                    for (SubCategoryModel subCategoryBean : subCategoryModelList) {

                        productsData(subCategoryBean.getSubcategory(), subCategoryBean.getmKey());

                    }

                    txtError.setVisibility(View.GONE);

                }else {
                    txtError.setText("No Products Found !");
                    txtError.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                simpleSwipeRefreshLayout.setRefreshing(false);
            }
        });


    }

    private void productsData(String title, String subCatmKey) {

        // header title
        HeaderSectionDataModel dm = new HeaderSectionDataModel();
        dm.setHeaderTitle(title);
        dm.setSubCatMkey(subCatmKey);

        ArrayList<SingleItemModel> singleItem = new ArrayList<SingleItemModel>();

        simpleSwipeRefreshLayout.setRefreshing(true);
        productsModelList = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_PRODUCTS).child(CatMKey).child(subCatmKey);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                simpleSwipeRefreshLayout.setRefreshing(false);

                Log.e("SNAPSHOTproductsData", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    productsModelList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        ProductsModel upload = postSnapshot.getValue(ProductsModel.class);
                        upload.setmKey(postSnapshot.getKey());
                        productsModelList.add(upload);

                    }

                    for (ProductsModel productsModel : productsModelList) {

                        singleItem.add(new SingleItemModel(productsModel.getId(), productsModel.getCurrentDate(), productsModel.getImageType(),
                                productsModel.getImageUrl(), productsModel.getCategory(), productsModel.getSubcategory(),
                                productsModel.getProductName(), productsModel.getProductPrice(), productsModel.getCapacityWeight(),
                                productsModel.getAvailability(), productsModel.getDescription(), productsModel.getmKey()));
                    }

                    dm.setAllItemsInSection(singleItem);
                    allSampleData.add(dm);

                    // product Recycler
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                    myRecyclerView.setLayoutManager(mLayoutManager1);
                    myRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    myRecyclerView.setHasFixedSize(true);
                    myRecyclerView.setNestedScrollingEnabled(false);

                    // product Adapter
                    subCatRecyclerViewDataAdapter = new SubCatRecyclerViewDataAdapter(getApplicationContext(), allSampleData, CatMKey);
                    myRecyclerView.setAdapter(subCatRecyclerViewDataAdapter);
                    subCatRecyclerViewDataAdapter.notifyDataSetChanged();

                    txtError.setVisibility(View.GONE);
                } else {

                    txtError.setText("No Products Found !");
                    txtError.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                simpleSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }


    @Override
    protected void onRestart() {

        appController.cartCount(userId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onRestart();
    }

    @Override
    protected void onStart() {
        appController.cartCount(userId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onStart();
    }

    ///
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setIcon(Converter.convertLayoutToImage(GroceryHomeActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_cart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;


        }
        return super.onOptionsItemSelected(item);
    }


}
