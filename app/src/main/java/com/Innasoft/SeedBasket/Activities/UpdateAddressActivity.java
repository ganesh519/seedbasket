package com.Innasoft.SeedBasket.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.Innasoft.SeedBasket.Model.AddAddressModel;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Singleton.AppController;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;

import com.Innasoft.SeedBasket.Utilities.ConnectivityReceiver;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_ADDRESS;


public class UpdateAddressActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    PrefManagerUser pref;
    String userId, module, checkId,  deviceId, addressid, username, mobile, addressline1, addressline2, area, city, state, picode, altenateno, set_as_default;
    int subTotal;
    AppController appController;
    boolean checkoutStatus;
    private static final String TAG = "AddAddressActivity";
    @BindView(R.id.textSearch)
    TextView textSearch;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.name_til)
    TextInputLayout nameTil;
    @BindView(R.id.etAddressline1)
    EditText etAddressline1;
    @BindView(R.id.adreess1_til)
    TextInputLayout adreess1Til;
    @BindView(R.id.etAddressline2)
    EditText etAddressline2;
    @BindView(R.id.adreess2_til)
    TextInputLayout adreess2Til;
    @BindView(R.id.etArea)
    EditText etArea;
    @BindView(R.id.area_til)
    TextInputLayout areaTil;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.city_till)
    TextInputLayout cityTill;
    @BindView(R.id.etState)
    EditText etState;
    @BindView(R.id.state_til)
    TextInputLayout stateTil;
    @BindView(R.id.etCountry)
    EditText etCountry;
    @BindView(R.id.country_til)
    TextInputLayout countryTil;
    @BindView(R.id.etPincode)
    EditText etPincode;
    @BindView(R.id.zip_til)
    TextInputLayout zipTil;
    @BindView(R.id.etPhoneNo)
    EditText etPhoneNo;
    @BindView(R.id.mobile_til)
    TextInputLayout mobileTil;
    @BindView(R.id.etAlternateno)
    EditText etAlternateno;
    @BindView(R.id.ti_etAlternateno)
    TextInputLayout tiEtAlternateno;
    @BindView(R.id.checkboxDefault)
    CheckBox checkboxDefault;
    @BindView(R.id.btnApply)
    Button btnApply;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_address);

        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Update Address");


        pref = new PrefManagerUser(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");

        if (getIntent().getExtras() != null) {
            subTotal = getIntent().getIntExtra("subTotal",0);
            module = getIntent().getStringExtra("module");
            addressid = getIntent().getStringExtra("addressId");
            Log.d(TAG, "onCreate: " + addressid);
            username = getIntent().getStringExtra("username");
            mobile = getIntent().getStringExtra("mobile");
            addressline1 = getIntent().getStringExtra("addressline1");
            addressline2 = getIntent().getStringExtra("addressline2");
            area = getIntent().getStringExtra("area");
            city = getIntent().getStringExtra("city");
            state = getIntent().getStringExtra("state");
            picode = getIntent().getStringExtra("pincode");
            altenateno = getIntent().getStringExtra("altenateno");
            set_as_default = getIntent().getStringExtra("setdefault");
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);
        }


        appController = (AppController) getApplication();


        etName.setText(username);
        etPhoneNo.setText(mobile);
        etAddressline1.setText(addressline1);
        etAddressline2.setText(addressline2);
        etArea.setText(area);
        etCity.setText(city);
        etState.setText(state);
        etPincode.setText(picode);
        etCountry.setText("India");
        etAlternateno.setText(altenateno);

        etName.addTextChangedListener(new MyTextWatcher(etName));
        etAddressline1.addTextChangedListener(new MyTextWatcher(etAddressline1));
        etAddressline2.addTextChangedListener(new MyTextWatcher(etAddressline2));
        etArea.addTextChangedListener(new MyTextWatcher(etArea));
        etCity.addTextChangedListener(new MyTextWatcher(etCity));
        etState.addTextChangedListener(new MyTextWatcher(etState));
        etCountry.addTextChangedListener(new MyTextWatcher(etCountry));
        etPincode.addTextChangedListener(new MyTextWatcher(etPincode));
        etPhoneNo.addTextChangedListener(new MyTextWatcher(etPhoneNo));
        etAlternateno.addTextChangedListener(new MyTextWatcher(etAlternateno));


        if (set_as_default.equalsIgnoreCase("Yes")) {

            checkboxDefault.setChecked(true);
            checkId = "Yes";
            Log.e("CHECKiD:", "" + checkId);

        } else {
            checkboxDefault.setChecked(false);
            checkId = "No";
            Log.e("CHECKiD2:", "" + checkId);

        }
        checkboxDefault.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    checkId = "Yes";
                } else {

                    checkId = "No";
                }
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @OnClick(R.id.btnApply)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnApply:

                boolean isConnected = appController.isConnection();
                if (isConnected) {
                    validateForm();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;
                    snackBar(message, color);
                }

                break;
        }
    }

    private void validateForm() {
        progressBar.setVisibility(View.VISIBLE);

        String name = etName.getText().toString();
        String addressline1 = etAddressline1.getText().toString();
        String addressline2 = etAddressline2.getText().toString();
        String area = etArea.getText().toString();
        String city = etCity.getText().toString();
        String state = etState.getText().toString();
        String country = etCountry.getText().toString();
        String pincode = etPincode.getText().toString();
        String phone = etPhoneNo.getText().toString();
        String alternateno = etAlternateno.getText().toString();





        if ((!isValidName(name))) {
            return;
        }
        if (!isValidPinCode(pincode)) {
            return;
        }
        if ((!isValidPhoneNumber(phone))) {
            return;
        }
        /*if ((!isValidPhoneNumber1(alternateno))) {
            return;
        }
*/
        AddAddressModel addAddressModel=new AddAddressModel();
        addAddressModel.setName(name);
        addAddressModel.setAddress1(addressline1);
        addAddressModel.setAddress2(addressline2);
        addAddressModel.setArea(area);
        addAddressModel.setCity(city);
        addAddressModel.setCountry(country);
        addAddressModel.setPincode(pincode);
        addAddressModel.setPhone(phone);
        addAddressModel.setState(state);
        addAddressModel.setAlterno(alternateno);
        addAddressModel.setCheck(checkId);
        addAddressModel.setKey(addressid);

        mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_ADDRESS).child(userId).child(addressid);
        // String uploadId = mDatabase.push().getKey();
//        DatabaseReference newRef = mDatabase.push();

        mDatabase.setValue(addAddressModel);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                Intent intent = new Intent(UpdateAddressActivity.this, AddressListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Checkout", checkoutStatus);
                intent.putExtra("addressId", addressid);
                intent.putExtra("subTotal", subTotal);
                startActivity(intent);

                progressBar.setVisibility(View.GONE);


            }
        }, 1000);


    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etName:
                    isValidName(etName.getText().toString().trim());
                    break;
                case R.id.etPhoneNo:
                    isValidPhoneNumber(etPhoneNo.getText().toString().trim());
                    break;
                case R.id.etPincode:
                    isValidPinCode(etPincode.getText().toString().trim());
                    break;
                case R.id.etAlternateno:
                    isValidPhoneNumber1(etAlternateno.getText().toString().trim());
                    break;

            }
        }
    }

    private boolean isValidName(String name) {
        Pattern pattern = Pattern.compile("[a-zA-Z ]+");
        Matcher matcher = pattern.matcher(name);

        if (name.isEmpty()) {
            nameTil.setError("name is required");
            requestFocus(etName);
            return false;
        } else if (!matcher.matches()) {
            nameTil.setError("Enter Alphabets Only");
            requestFocus(etName);
            return false;
        } else if (name.length() < 5 || name.length() > 20) {
            nameTil.setError("Name Should be 5 to 20 characters");
            requestFocus(etName);
            return false;
        } else {
            nameTil.setErrorEnabled(false);
        }
        return matcher.matches();
    }


    // validate phone
    private boolean isValidPhoneNumber(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            mobileTil.setError("Phone no is required");
            requestFocus(etPhoneNo);
            return false;
        } else if (!matcher.matches()) {
            mobileTil.setError("Enter a valid mobile");
            requestFocus(etPhoneNo);
            return false;
        } else {
            mobileTil.setErrorEnabled(false);
        }

        return matcher.matches();
    }

    private boolean isValidPhoneNumber1(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            tiEtAlternateno.setError("Phone no is required");
            requestFocus(etAlternateno);
            return false;
        } else if (!matcher.matches()) {
            tiEtAlternateno.setError("Enter a valid mobile");
            requestFocus(etAlternateno);
            return false;
        } else {
            tiEtAlternateno.setErrorEnabled(false);
        }

        return matcher.matches();
    }

    // valid OTP
    private boolean isValidPinCode(String pincode) {


        if (pincode.isEmpty()) {
            zipTil.setError("Pincode is required");
            requestFocus(etPincode);
            etCity.setText("");
            etState.setText("");
            return false;
        } else if (pincode.length() < 6) {
            zipTil.setError("Enter a valid Pincode");

            return false;
        } else {
            zipTil.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }

    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
//        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(color);
        snackbar.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                /*Intent intent =new Intent(CartActivity.this, SmsActivity.class);
                intent.putExtra("link",carBikeItem.getLink());
                intent.putExtra("id",carBikeItem.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);*/
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
