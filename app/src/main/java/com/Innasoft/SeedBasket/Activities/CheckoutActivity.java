package com.Innasoft.SeedBasket.Activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.Innasoft.SeedBasket.Adapters.PaymentTpyeRecyclerAdapter;
import com.Innasoft.SeedBasket.Interface.PaymentTypeInterface;
import com.Innasoft.SeedBasket.Model.AddAddressModel;
import com.Innasoft.SeedBasket.Model.PaymentModeModel;
import com.Innasoft.SeedBasket.Model.PostOrderModel;
import com.Innasoft.SeedBasket.Model.Product;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Singleton.AppController;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;
import com.Innasoft.SeedBasket.Utilities.Constants;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_ADDRESS;
import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_CART;
import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_ORDERS;

public class CheckoutActivity extends AppCompatActivity implements PaymentTypeInterface {
    public static final String TAG = "CheckoutActivity : ";
    @BindView(R.id.txtShippingAddress)
    TextView txtShippingAddress;
    @BindView(R.id.txtChangeAddress)
    TextView txtChangeAddress;
    @BindView(R.id.txtUserName)
    TextView txtUserName;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtOrderDetails)
    TextView txtOrderDetails;
    @BindView(R.id.textView9)
    TextView textView9;
    @BindView(R.id.txtItems)
    TextView txtItems;
    @BindView(R.id.textView12)
    TextView textView12;
    @BindView(R.id.txtSubTotal)
    TextView txtSubTotal;
    @BindView(R.id.textView15)
    TextView textView15;
    @BindView(R.id.txtDiscount)
    TextView txtDiscount;
    @BindView(R.id.textView17)
    TextView textView17;
    @BindView(R.id.txtGrandTotal)
    TextView txtGrandTotal;
    @BindView(R.id.txt145)
    TextView txt145;
    @BindView(R.id.txtDeliver)
    TextView txtDeliver;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.textView21)
    TextView textView21;
    @BindView(R.id.txtAmountTobepaid)
    TextView txtAmountTobepaid;
    @BindView(R.id.orderRelative)
    RelativeLayout orderRelative;
    @BindView(R.id.txtPaymentMode)
    TextView txtPaymentMode;
    @BindView(R.id.recycler_paymentoptions)
    RecyclerView recyclerPaymentoptions;
    @BindView(R.id.btnOrder)
    Button btnOrder;
    @BindView(R.id.bottamlayout)
    RelativeLayout bottamlayout;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;

    AppController appController;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    ArrayList<PaymentModeModel> paymentModeModels;
    ArrayList<Product> productArrayList;

    private PrefManagerUser pref;
    PaymentTpyeRecyclerAdapter paymentTpyeRecyclerAdapter;
    PaymentTypeInterface paymentTypeInterface;
    private DatabaseReference mDatabase;
    String userId, addressId, paymentId;
    int subTotal;
    boolean cartStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("CheckOut");

        appController = (AppController) getApplication();
        paymentTypeInterface = (PaymentTypeInterface) this;
        pref = new PrefManagerUser(getApplicationContext());
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");

        if (getIntent() != null) {
            cartStatus = getIntent().getBooleanExtra("Checkout", false);
            subTotal = getIntent().getIntExtra("subTotal", 0);
            addressId = getIntent().getStringExtra("addressId");

        }

        txtSubTotal.setText("\u20B9" + subTotal + ".00");
        txtDiscount.setText("\u20B9" + "0.00");
        txtGrandTotal.setText("\u20B9" + subTotal + ".00");
        txtDeliver.setText("Free Delivery");
        txtAmountTobepaid.setText("\u20B9" + subTotal + ".00");


        txtChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CheckoutActivity.this, AddressListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Checkout", cartStatus);
                intent.putExtra("addressId", addressId);
                intent.putExtra("subTotal", subTotal);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        addressList();
        paymentMode();


        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (addressId == null) {
                    Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + "Please Choose Address" + "</font>"), Snackbar.LENGTH_SHORT).show();

                    //Toast.makeText(CheckoutActivity.this, "Please Choose Address ", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (paymentId == null) {
                    Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + "Please Select Payment method" + "</font>"), Snackbar.LENGTH_SHORT).show();

                    //Toast.makeText(CheckoutActivity.this, "Please Select Payment method ", Toast.LENGTH_SHORT).show();
                    return;
                }

                postOrder();

            }
        });
    }

    private void addressList() {
        progressBar.setVisibility(View.VISIBLE);
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_ADDRESS).child(userId).child(addressId);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog

                progressBar.setVisibility(View.GONE);
                if (snapshot != null) {
                    txtUserName.setText("" + snapshot.child("name").getValue());
                    txtAddress.setText(snapshot.child("address1").getValue() + "," + snapshot.child("address2").getValue() + "," + snapshot.child("area").getValue() + "," + snapshot.child("city").getValue() + "," +
                            snapshot.child("state").getValue() + "," + snapshot.child("pincode").getValue());
                } else {
                    txtAddress.setText("Please Add Address !! ");
                    txtAddress.setTextColor(Color.RED);
                    txtAddress.setTextSize(20);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);


            }
        });
    }

    private void paymentMode() {
        progressBar.setVisibility(View.VISIBLE);
        paymentModeModels = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_PAMENTMODE);
        mDatabase.keepSynced(true);


        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog

                progressBar.setVisibility(View.GONE);
                if (snapshot != null) {

                    paymentModeModels.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        PaymentModeModel upload = postSnapshot.getValue(PaymentModeModel.class);
                        // upload.setmKey(postSnapshot.getKey());
                        paymentModeModels.add(upload);

                    }

                    // payment gateway array
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    recyclerPaymentoptions.setLayoutManager(layoutManager);
                    recyclerPaymentoptions.setItemAnimator(new DefaultItemAnimator());

                    paymentTpyeRecyclerAdapter = new PaymentTpyeRecyclerAdapter(CheckoutActivity.this, paymentModeModels, paymentTypeInterface);
                    recyclerPaymentoptions.setAdapter(paymentTpyeRecyclerAdapter);

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(List<PaymentModeModel> paymentGatewayBeans, int i) {

        paymentId = paymentGatewayBeans.get(i).getPaymentId();
        Log.d(TAG, "onItemClick: " + paymentId);

    }

    private void postOrder() {

        progressBar.setVisibility(View.VISIBLE);
        mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_CART).child(userId);
        mDatabase.keepSynced(true);
        productArrayList = new ArrayList<>();

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog

                progressBar.setVisibility(View.GONE);

                if (snapshot != null) {
                    productArrayList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Product upload = postSnapshot.getValue(Product.class);
                        upload.setmKey(postSnapshot.getKey());
                        productArrayList.add(upload);
                    }

                    int randaom = new Random().nextInt(60000) + 20000;
                    String referenceId = "SB" + randaom;
                    Log.d(TAG, "onDataChange: " + referenceId);

                    mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_ORDERS).child(userId);
                    String orderId = mDatabase.push().getKey();
                    PostOrderModel postOrderModel = new PostOrderModel();
                    postOrderModel.setAddressId(addressId);
                    postOrderModel.setPaymentId(paymentId);
                    postOrderModel.setUserId(userId);
                    postOrderModel.setPostKey(orderId);
                    postOrderModel.setReferenceId(referenceId);
                    mDatabase.child(orderId).setValue(postOrderModel);

                    mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_ORDERS).child(userId).child(orderId);
                    for (Product answer : productArrayList) {
                        mDatabase.push().setValue(answer);

                    }
                    deleteCartItem();
                    showCustomDialog(orderId, getResources().getString(R.string.orderMessage) + referenceId);


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);


            }
        });

    }

    // delete cart item
    private boolean deleteCartItem() {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_CART).child(userId);
        //removing artist
        dR.removeValue();


        // Toast.makeText(getApplicationContext(), "Post Deleted", Toast.LENGTH_LONG).show();

        return true;
    }


    private void showCustomDialog(final String order_id, String message) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomAlertDialog);
//finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        TextView txtmessage = (TextView) dialogView.findViewById(R.id.txtMessage);
        txtmessage.setText(message);


        Button buttonOk = (Button) dialogView.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent order_detail = new Intent(CheckoutActivity.this, OrdersListActivity.class);
                order_detail.putExtra("Order_ID", order_id);
                order_detail.putExtra("Checkout", cartStatus);
                order_detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(order_detail);
                finish();*/


                alertDialog.dismiss();
            }
        });


    }

}
