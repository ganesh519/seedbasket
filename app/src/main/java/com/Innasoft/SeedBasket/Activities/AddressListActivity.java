package com.Innasoft.SeedBasket.Activities;

import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import com.Innasoft.SeedBasket.Adapters.AddressAdapter;
import com.Innasoft.SeedBasket.Model.AddAddressModel;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Singleton.AppController;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_ADDRESS;


public class AddressListActivity extends AppCompatActivity {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    LocationManager locationManager;

    @BindView(R.id.btnAddAddress)
    Button btnAddAddress;
    @BindView(R.id.addressList)
    ListView addressList;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.txtAlert)
    TextView txtAlert;

    private PrefManagerUser pref;
    private AddressAdapter cartAdapter;
    ArrayList<AddAddressModel> productArrayList;
    private DatabaseReference mDatabase;

    String userId, addressId, customerId, totalPrice, tokenValue, deviceId;
    int checked,subTotal;
    boolean checkoutStatus;
//    ArrayList<AddressModelItem> addressModelItems = new ArrayList<>();
    private String provider = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Address");


        AppController app = (AppController) getApplication();
        pref = new PrefManagerUser(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        if (getIntent().getExtras() != null) {
            addressId = getIntent().getStringExtra("addressId");
            subTotal = getIntent().getIntExtra("subTotal",0);
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);

        }


        if (app.isConnection()) {

            prepareAddressData();


        } else {

            setContentView(R.layout.internet);


        }
    }

    private void prepareAddressData() {
        progress.setVisibility(View.VISIBLE);
        productArrayList = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_ADDRESS).child(userId);
        mDatabase.keepSynced(true);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                progress.setVisibility(View.GONE);


                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    productArrayList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        AddAddressModel upload = postSnapshot.getValue(AddAddressModel.class);
                        upload.setKey(postSnapshot.getKey());
                        productArrayList.add(upload);

                    }
                    cartAdapter = new AddressAdapter(AddressListActivity.this, productArrayList,checkoutStatus,subTotal);
                    addressList.setAdapter(cartAdapter);
                    cartAdapter.setSelectedIndex(checked);
                    addressList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            cartAdapter.setSelectedIndex(position);
                            cartAdapter.notifyDataSetChanged();
                        }
                    });


                }




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progress.setVisibility(View.GONE);            }
        });





//
    }

    @OnClick(R.id.btnAddAddress)
    public void onViewClicked() {
        Intent intent = new Intent(AddressListActivity.this, AddAddressActivity.class);
        intent.putExtra("Checkout", checkoutStatus);
        intent.putExtra("addressId", addressId);
        intent.putExtra("subTotal", subTotal);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                if (checkoutStatus) {
                    Intent intent = new Intent(AddressListActivity.this, CheckoutActivity.class);
                    intent.putExtra("Checkout", checkoutStatus);
                    intent.putExtra("addressId", addressId);
                    intent.putExtra("subTotal", subTotal);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(AddressListActivity.this, MyAccountActivity.class);
                    intent.putExtra("Checkout", checkoutStatus);
                    intent.putExtra("addressId", addressId);
                    intent.putExtra("subTotal", subTotal);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        if (checkoutStatus) {
            Intent intent = new Intent(AddressListActivity.this, CheckoutActivity.class);
            intent.putExtra("Checkout", checkoutStatus);
            intent.putExtra("addressId", addressId);
            intent.putExtra("subTotal", subTotal);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Intent intent = new Intent(AddressListActivity.this, MyAccountActivity.class);
            intent.putExtra("Checkout", checkoutStatus);
            intent.putExtra("addressId", addressId);
            intent.putExtra("subTotal", subTotal);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
