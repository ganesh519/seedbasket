package com.Innasoft.SeedBasket.Activities;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.Innasoft.SeedBasket.Adapters.WishListAdapter;
import com.Innasoft.SeedBasket.Interface.Cartinterface;
import com.Innasoft.SeedBasket.Interface.WishListInterface;
import com.Innasoft.SeedBasket.Model.CartorWistStatus;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;
import com.Innasoft.SeedBasket.Model.Product;
import com.Innasoft.SeedBasket.Utilities.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_CART;
import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_WISHLIST;

public class WishListActivity extends AppCompatActivity implements WishListInterface{
    private static final String TAG = "WishListActivity";
    @BindView(R.id.recyclerWishlist)
    RecyclerView recyclerWishlist;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private PrefManagerUser pref;
    String userId;
    ArrayList<Product> productArrayList;
    private DatabaseReference mDatabase;
    WishListAdapter wishListAdapter;
    WishListInterface wishListInterface;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);

        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My WishList");

        pref = new PrefManagerUser(getApplicationContext());
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");

        wishListInterface = (WishListInterface) this;

        preparewishlistData();
    }

    private void preparewishlistData() {
        progressBar.setVisibility(View.VISIBLE);

        productArrayList = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_WISHLIST).child(userId);
        mDatabase.keepSynced(true);


        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                progressBar.setVisibility(View.GONE);

                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    productArrayList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Product upload = postSnapshot.getValue(Product.class);
                        upload.setmKey(postSnapshot.getKey());
                        productArrayList.add(upload);

                    }
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(WishListActivity.this, LinearLayoutManager.VERTICAL, false);
                    recyclerWishlist.setLayoutManager(mLayoutManager1);

                    wishListAdapter = new WishListAdapter(WishListActivity.this, productArrayList,wishListInterface);
                    recyclerWishlist.setAdapter(wishListAdapter);
                    recyclerWishlist.setItemAnimator(new DefaultItemAnimator());
                    wishListAdapter.notifyDataSetChanged();


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onremoveFavorite(Product product) {

        deleteCartItem(product.getmKey(), product);
        updateCart(userId, product);
    }


    // delete cart item
    private boolean deleteCartItem(String mKey, Product product) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_WISHLIST).child(userId).child(mKey);
        //removing artist
        dR.removeValue();
        productArrayList.remove(product);
        wishListAdapter.notifyDataSetChanged();

        return true;
    }

    // update cart status
    private void updateCart(String userId, Product product) {

        Log.d(TAG, "updateCart: "+product.getmKey()+"---"+product.getSubCatMKey()+"---"+product.getCatMKey());
        //getting the specified artist reference
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.CART_WISH_STATUS).child(product.catMKey).child(product.subCatMKey).child(product.id);

        //updating artist
        CartorWistStatus cartorWistStatus = new CartorWistStatus();
        //Adding values
        cartorWistStatus.setUserId(userId);
        cartorWistStatus.setCartStatus(product.cartStatus);
        cartorWistStatus.setWishlistStatus("0");
        // CartorWistStatus product = new CartorWistStatus(userId, cartStatus, wishStatus);
        mDatabase.child(userId).setValue(cartorWistStatus);


    }

}
