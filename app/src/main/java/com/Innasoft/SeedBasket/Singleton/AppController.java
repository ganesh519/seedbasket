package com.Innasoft.SeedBasket.Singleton;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.Innasoft.SeedBasket.Utilities.ConnectivityReceiver;
import com.Innasoft.SeedBasket.Utilities.Constants;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;
import com.Innasoft.SeedBasket.Model.Product;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AppController extends Application {
    public static final String TAG = AppController.class
            .getSimpleName();


    private static AppController mInstance;

    private PrefManagerUser pref;
    private static final String PREFS_NAME = "CARTCOUNT";

    ArrayList<Product> productArrayList;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        if (!FirebaseApp.getApps(this).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }


    }
    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public boolean isConnection(){

        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();

        return networkInfo !=null && networkInfo.isConnected();
    }



    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public void cartCount(final String userId) {


        productArrayList = new ArrayList<>();

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CART).child(userId);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {


                Log.e("CARTCOUNT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    productArrayList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Product upload = postSnapshot.getValue(Product.class);
                        upload.setmKey(postSnapshot.getKey());
                        productArrayList.add(upload);

                    }

                    int cartindex = productArrayList.size();

                    if (cartindex > 0) {
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("itemCount", cartindex);
                        editor.apply();

                    } else {
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("itemCount", 0);
                        editor.apply();
                    }


                }else {
                    SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("itemCount", 0);
                    editor.apply();
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
