package com.Innasoft.SeedBasket.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.Innasoft.SeedBasket.Interface.Cartinterface;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Model.Product;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {


    private Context mContext;
    private List<Product> productsModelList;
    private Cartinterface cartinterface;
    String catMKey, subcatMkey;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumbnail1)

        ImageView thumbnail1;
        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.product_unit)
        TextView productUnit;
        @BindView(R.id.product_Price)
        TextView productPrice;
        @BindView(R.id.final_product_Price)
        TextView finalProductPrice;
        @BindView(R.id.img_remove)
        ImageView imgRemove;
        @BindView(R.id.product_minus)
        TextView productMinus;
        @BindView(R.id.product_quantity)
        TextView productQuantity;
        @BindView(R.id.product_plus)
        TextView productPlus;
        @BindView(R.id.btnFavourite)
        TextView btnFavourite;
        @BindView(R.id.linearSaveItem)
        LinearLayout linearSaveItem;
        @BindView(R.id.btnRemove)
        TextView btnRemove;
        @BindView(R.id.linearremove)
        LinearLayout linearremove;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public CartAdapter(Context mContext, List<Product> productsModelList, Cartinterface cartinterface, String catMKey, String subcatMkey) {
        this.mContext = mContext;
        this.productsModelList = productsModelList;
        this.cartinterface = cartinterface;
        this.catMKey = catMKey;
        this.subcatMkey = subcatMkey;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_product, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Product productsModel = productsModelList.get(position);

        // loading album cover using Glide library
        Glide.with(mContext).load(productsModel.getImageUrl()).into(holder.thumbnail1);
        Log.e(TAG, "onBindViewHolder: " + productsModel.getId());

        holder.productName.setText(productsModel.getProductName());
        holder.productQuantity.setText("" + productsModel.getQuantity());
        holder.productPrice.setText(mContext.getResources().getString(R.string.Rs) + productsModel.getProductPrice());

        holder.productMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cartinterface.onMinusClick(productsModel);
            }
        });


        holder.productPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cartinterface.onPlusClick(productsModel);
            }
        });


        holder.linearSaveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cartinterface.onWishListDialog(productsModel);
            }
        });

        holder.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cartinterface.onRemoveDialog(productsModel);
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return productsModelList.size();
    }


}