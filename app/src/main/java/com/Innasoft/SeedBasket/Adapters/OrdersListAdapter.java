package com.Innasoft.SeedBasket.Adapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.Innasoft.SeedBasket.R;
import com.bumptech.glide.Glide;



public class OrdersListAdapter extends RecyclerView.Adapter<OrdersListAdapter.MyViewHolder> {
    private Context mContext;
//    List<OrderListResponse.DataBean.ProductsBean> productsBeanList;
//    public OrdersListAdapter(OrdersListActivity ordersListActivity, List<OrderListResponse.DataBean.ProductsBean> productsBeanList) {
//        this.mContext=ordersListActivity;
//        this.productsBeanList=productsBeanList;
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imgOrder;
        TextView txtOrderName,txtOrderID,txtOrderPrice,txtStatus,txtReview,txtQty;
        LinearLayout reviewLayout;


        public MyViewHolder(View itemView) {
            super(itemView);

            imgOrder = (ImageView)itemView.findViewById(R.id.imgOrder);
            txtOrderName = (TextView)itemView.findViewById(R.id.txtOrderName);
            txtOrderID = (TextView)itemView.findViewById(R.id.txtOrderID);
            txtOrderPrice = (TextView)itemView.findViewById(R.id.txtOrderPrice);
            txtQty=(TextView)itemView.findViewById(R.id.txtQty);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.myorderlistmodel1, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

//        Glide.with(mContext).load(PRODUCT_IMAGE_BASE_URL2+productsBeanList.get(position).getImages()).error(R.drawable.default_loading).into(holder.imgOrder);
//
//        holder.txtOrderName.setText(capitalize(productsBeanList.get(position).getProductName()));
////        holder.txtOrderID.setText(productsBeanList.get(position).getProductId());
//        holder.txtQty.setText("Product Quantity : "+productsBeanList.get(position).getPurchaseQuantity());
//        holder.txtOrderPrice.setText(mContext.getResources().getString(R.string.Rs)+" "+ productsBeanList.get(position).getTotalPrice());


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 0;
    }


}
