package com.Innasoft.SeedBasket.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.Innasoft.SeedBasket.Interface.WishListInterface;
import com.Innasoft.SeedBasket.Model.Product;
import com.Innasoft.SeedBasket.R;
import com.bumptech.glide.Glide;


import java.util.ArrayList;
import java.util.List;


public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.MyViewHolder> {

    private Context mContext;
    ArrayList<Product> cartListBeanList;
    WishListInterface wishListInterface;


    public WishListAdapter(Context mContext, ArrayList<Product> cartListBeanList, WishListInterface wishListInterface) {
        this.mContext = mContext;
        this.cartListBeanList = cartListBeanList;
        this.wishListInterface = wishListInterface;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemName, txtPrice, btnDelete;
        public ImageView thumbnail;
        public LinearLayout parentLayout;


        public MyViewHolder(View view) {
            super(view);
            txtItemName = (TextView) view.findViewById(R.id.item_name);
            txtPrice = (TextView) view.findViewById(R.id.item_price);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            btnDelete = (TextView) view.findViewById(R.id.remove_item);
            parentLayout = (LinearLayout) view.findViewById(R.id.parentLayout);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.wish_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Product wishListBean = cartListBeanList.get(position);
//
        holder.txtItemName.setText(cartListBeanList.get(position).getProductName());
        holder.txtPrice.setText(" \u20B9" + cartListBeanList.get(position).getProductPrice());
        Glide.with(mContext).load(cartListBeanList.get(position).getImageUrl()).into(holder.thumbnail);


        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wishListInterface.onremoveFavorite(wishListBean);

            }
        });
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return cartListBeanList.size();
    }


    private void delete(final View view, String id) {



    }


}