package com.Innasoft.SeedBasket.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.Innasoft.SeedBasket.Model.SubCategoryModel;
import com.Innasoft.SeedBasket.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.Innasoft.SeedBasket.Activities.ProductListActivity;


import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class SubCatageoryAdapter extends RecyclerView.Adapter<SubCatageoryAdapter.MyViewHolder> {
    private Context mContext;
    private String CatMkey;
    private List<SubCategoryModel> homeList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView thumbnail;
        public TextView txtName;
        public LinearLayout linearLayout;


        public MyViewHolder(View view) {
            super(view);

            thumbnail = (CircleImageView) view.findViewById(R.id.thumbnail);
            txtName = (TextView) view.findViewById(R.id.txtCatName);
            linearLayout = (LinearLayout) view.findViewById(R.id.parentLayout);


        }
    }

    public SubCatageoryAdapter(Context mContext, List<SubCategoryModel> homekitchenList, String catMKey) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
        this.CatMkey = catMKey;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subcat_home_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final SubCategoryModel home = homeList.get(position);

        // loading album cover using Glide library
        Glide.with(mContext).load(home.getImageUrl()).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.thumbnail);

        holder.txtName.setText(home.getSubcategory());

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                    Activity activity = (Activity) mContext;
                Intent intent = new Intent(mContext, ProductListActivity.class);
                   /* if (home.getModule().equalsIgnoreCase("celebrations")){
                        intent.putExtra("catId", home.getId()+"&price="+"high");
                    } else {
                        intent.putExtra("catId", home.getId());
                    }*/
                intent.putExtra("CatMKey", CatMkey);
                intent.putExtra("subcatMkey", home.getmKey());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                // activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                SharedPreferences preferences1 = mContext.getSharedPreferences("SORT", 0);
                SharedPreferences.Editor editor = preferences1.edit();
                editor.putString("catId", home.getmKey());
                editor.putString("title", home.getSubcategory());
                editor.putString("subcatId", home.getId());
                editor.putString("sort", "Price High to Low");
                editor.apply();


            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


}