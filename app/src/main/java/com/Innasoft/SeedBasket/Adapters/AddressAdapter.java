package com.Innasoft.SeedBasket.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;


import com.Innasoft.SeedBasket.Activities.AddressListActivity;
import com.Innasoft.SeedBasket.Activities.CheckoutActivity;
import com.Innasoft.SeedBasket.Model.AddAddressModel;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;
import com.Innasoft.SeedBasket.Activities.UpdateAddressActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;

import static com.Innasoft.SeedBasket.Utilities.Constants.DATABASE_PATH_ADDRESS;

public class AddressAdapter extends BaseAdapter {

    private int selectedIndex = -1;
    private Context mContext;
//    private List<AddressModelItem> cartListBeanList;
    private String key;
    private String userId;
    private int subTotal;
    boolean checkoutStatus;
    ArrayList<AddAddressModel> productArrayList;
    PrefManagerUser pref;

    public AddressAdapter(AddressListActivity addressListActivity, ArrayList<AddAddressModel> productArrayList, boolean checkoutStatus, int subTotal) {
        this.mContext=addressListActivity;
        this.productArrayList=productArrayList;
        this.checkoutStatus=checkoutStatus;
        this.subTotal=subTotal;
    }

//    public AddressAdapter(Context mContext, List<AddressModelItem> cartListBeanList, String tokenValue, String userId, boolean checkoutStatus) {
//        this.mContext = mContext;
//        this.cartListBeanList = cartListBeanList;
//        this.tokenValue = tokenValue;
//        this.userId = userId;
//        this.checkoutStatus = checkoutStatus;
//    }

    @Override
    public int getCount() {
        return productArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return 0;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View view1 = View.inflate(mContext, R.layout.address_card,null);


//        final AddressModelItem cartListBean = cartListBeanList.get(i);

//        Log.d("ADDID", "getView: "+cartListBean.getId());

        TextView txtName,txtAddress,txtMobile;
        Button btnEdit,btnDelete;
        RadioButton radioButton;
        Button btnDelivery;


        txtName = (TextView) view1.findViewById(R.id.txtName);
        txtAddress = (TextView) view1.findViewById(R.id.txtAddress);
        txtMobile = (TextView) view1.findViewById(R.id.txtMobile);
        btnEdit = (Button) view1.findViewById(R.id.txtEdit);
        btnDelete = (Button) view1.findViewById(R.id.btnDelete);
        radioButton=(RadioButton) view1.findViewById(R.id.radioButton);
        btnDelivery =(Button)view1.findViewById(R.id.btnDelivery);


        txtName.setText(productArrayList.get(i).getName());
        txtAddress.setText(productArrayList.get(i).getAddress1()+","+productArrayList.get(i).getAddress2()+","+productArrayList.get(i).getArea()+","+productArrayList.get(i).getCity()+","+
                productArrayList.get(i).getState()+","+productArrayList.get(i).getPincode());
        txtMobile.setText("Mobile : "+productArrayList.get(i).getPhone());
//
//        if (cartListBean.getAlternate_contact_no().equalsIgnoreCase("")){
//
//
//        }else {
//            txtMobile.setText("Mobile : "+cartListBean.getContact_no()+"\n"+"Alternate Contact no : "+cartListBean.getAlternate_contact_no());
//
//        }


        radioButton.setVisibility(View.GONE);
        btnDelivery.setVisibility(View.GONE);


        if(selectedIndex == i){

                radioButton.setChecked(true);
                btnEdit.setVisibility(View.VISIBLE);
                btnDelete.setVisibility(View.VISIBLE);


                if (checkoutStatus){
                    btnDelivery.setVisibility(View.VISIBLE);
                }
                else {

                }


                }
        else{
            radioButton.setChecked(false);
            btnEdit.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
            btnDelivery.setVisibility(View.GONE);
        }


        btnDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = (Activity) mContext;
                Intent intent =new Intent(mContext, CheckoutActivity.class);
                intent.putExtra("addressId",productArrayList.get(i).getKey());
                intent.putExtra("Checkout", checkoutStatus);
                intent.putExtra("subTotal", subTotal);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Activity activity = (Activity) mContext;

                Intent intent =new Intent(mContext, UpdateAddressActivity.class);
                intent.putExtra("subTotal",subTotal);
                intent.putExtra("addressId",productArrayList.get(i).getKey());
                intent.putExtra("city",productArrayList.get(i).getCity());
                intent.putExtra("username",productArrayList.get(i).getName());
                intent.putExtra("state",productArrayList.get(i).getState());
                intent.putExtra("mobile",productArrayList.get(i).getPhone());
                intent.putExtra("addressline1",productArrayList.get(i).getAddress1());
                intent.putExtra("addressline2",productArrayList.get(i).getAddress2());
                intent.putExtra("area",productArrayList.get(i).getArea());
                intent.putExtra("pincode",productArrayList.get(i).getPincode());
                intent.putExtra("altenateno",productArrayList.get(i).getAlterno());
                intent.putExtra("setdefault",productArrayList.get(i).getCheck());
                intent.putExtra("Checkout", checkoutStatus);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                // Toast.makeText(mContext,""+cartListBean.getAddressId(),Toast.LENGTH_SHORT).show();
            }
        });
        pref = new PrefManagerUser(mContext);
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.alert_dialog);


                // set the custom dialog components - text, image and button
                TextView te=(TextView)dialog.findViewById(R.id.txtAlert);
                te.setText("Are You Sure Want to Delete ?");

                TextView yes=(TextView) dialog.findViewById(R.id.btnYes);
                TextView no=(TextView) dialog.findViewById(R.id.btnNo);

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        key=productArrayList.get(i).getKey();
                        delete(userId,key,mContext);
                        productArrayList.remove(i);
                        notifyDataSetChanged();
//                        Log.e("COUNTADDRESS",""+cartListBeanList.size());

                       /* if (cartListBeanList.size()==0){
                            Intent intent = new Intent(mContext, AddressListActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("Checkout", checkoutStatus);
                            mContext.startActivity(intent);
                        }*/

                        dialog.dismiss();


                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });



        return view1;

    }

    public void setSelectedIndex(int index){
        selectedIndex = index;

    }


    public void delete(String userId, String id,Context context){

        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(DATABASE_PATH_ADDRESS).child(userId).child(id);
        //removing artist
        dR.removeValue();
        productArrayList.remove(productArrayList);
        Intent intent=new Intent(context,AddressListActivity.class);
        context.startActivity(intent);



    }

}