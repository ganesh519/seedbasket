package com.Innasoft.SeedBasket.Adapters;

import android.content.Context;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.Innasoft.SeedBasket.Activities.CheckoutActivity;
import com.Innasoft.SeedBasket.Interface.PaymentTypeInterface;
import com.Innasoft.SeedBasket.Model.PaymentModeModel;
import com.Innasoft.SeedBasket.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import static com.Innasoft.SeedBasket.Utilities.Constants.capitalize;


public class PaymentTpyeRecyclerAdapter extends RecyclerView.Adapter<PaymentTpyeRecyclerAdapter.Holder> {
    Context context;
    public int lastSelectedPosition = -1;
    public static String cash;
    List<PaymentModeModel> paymentGatewayBeans;
    private PaymentTypeInterface paymentTypeInterface;

    public PaymentTpyeRecyclerAdapter(CheckoutActivity checkoutActivity, ArrayList<PaymentModeModel> paymentGatewayBeans, PaymentTypeInterface paymentTypeInterface) {
        this.context = checkoutActivity;
        this.paymentGatewayBeans = paymentGatewayBeans;
        this.paymentTypeInterface = paymentTypeInterface;
    }

    @NonNull
    @Override
    public PaymentTpyeRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_paymentmethods, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PaymentTpyeRecyclerAdapter.Holder holder, final int i) {

        if (paymentGatewayBeans.get(i).isChecked()) {
            Log.d("dfgsadjhg", "onDataChange: " + paymentGatewayBeans.get(i));
            holder.parentLayout.setVisibility(View.VISIBLE);
        } else {
            holder.parentLayout.setVisibility(View.GONE);

        }

        holder.offer_name.setText(capitalize(paymentGatewayBeans.get(i).getPaymentType()));
        holder.offer_select.setChecked(lastSelectedPosition == i);

        Glide.with(context).load(paymentGatewayBeans.get(i).getImage()).into(holder.p_image);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSelectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();

                paymentTypeInterface.onItemClick(paymentGatewayBeans,i);
                /*Log.d("POSITION", "onClick: "+paymentGatewayBeans.);*/


                // cash = paymentGatewayBeans.get(lastSelectedPosition).getId();
            }
        };
        holder.itemView.setOnClickListener(clickListener);
        holder.offer_select.setOnClickListener(clickListener);

    }

    @Override
    public int getItemCount() {
        return paymentGatewayBeans.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        TextView offer_name;
        RelativeLayout parentLayout;
        RadioButton offer_select;
        ImageView p_image;

        public Holder(@NonNull final View itemView) {
            super(itemView);

            parentLayout = itemView.findViewById(R.id.parentLayout);
            offer_select = itemView.findViewById(R.id.offer_select);
            offer_name = itemView.findViewById(R.id.offer_name);
            p_image = itemView.findViewById(R.id.p_image);


        }
    }
}
