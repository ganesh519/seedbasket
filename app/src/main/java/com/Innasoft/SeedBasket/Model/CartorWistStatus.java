package com.Innasoft.SeedBasket.Model;

import com.google.firebase.database.Exclude;

public class CartorWistStatus {
    String userId;
    String cartStatus;
    String wishlistStatus;
    String mKey;

    public CartorWistStatus() {
    }

    public CartorWistStatus(String userId, String cartStatus, String wishlistStatus) {
        this.userId = userId;
        this.cartStatus = cartStatus;
        this.wishlistStatus = wishlistStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCartStatus() {
        return cartStatus;
    }

    public void setCartStatus(String cartStatus) {
        this.cartStatus = cartStatus;
    }

    public String getWishlistStatus() {
        return wishlistStatus;
    }

    public void setWishlistStatus(String wishlistStatus) {
        this.wishlistStatus = wishlistStatus;
    }

    @Exclude
    public String getmKey() {
        return mKey;
    }

    @Exclude
    public void setmKey(String mKey) {
        this.mKey = mKey;
    }
}
