package com.Innasoft.SeedBasket.Model;

public class PaymentModeModel {


    String paymentType;
    boolean checked;
    String paymentId;
    String image;


    public PaymentModeModel() {
    }

    public PaymentModeModel(String paymentType, boolean checked, String paymentId, String image) {
        this.paymentType = paymentType;
        this.checked = checked;
        this.paymentId = paymentId;
        this.image = image;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
