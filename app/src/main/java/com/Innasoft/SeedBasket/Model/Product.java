package com.Innasoft.SeedBasket.Model;

public class Product {
    public String userId;
    public String id;
    public String currentDate;
    public String imageType;
    public String imageUrl;
    public String category;
    public String subcategory;
    public String productName;
    public double productPrice;
    public String capacityWeight;
    public String availability;
    public String description;
    public String mKey;
    public int quantity;
    public String catMKey;
    public String subCatMKey;
    public String cartStatus;
    public String wishStatus;

    public Product() {
    }

    public Product(String userId, String id, String currentDate, String imageType, String imageUrl, String category, String subcategory, String productName, double productPrice, String capacityWeight, String availability, String description, String mKey, int quantity, String catMKey, String subCatMKey, String cartStatus, String wishStatus) {
        this.userId = userId;
        this.id = id;
        this.currentDate = currentDate;
        this.imageType = imageType;
        this.imageUrl = imageUrl;
        this.category = category;
        this.subcategory = subcategory;
        this.productName = productName;
        this.productPrice = productPrice;
        this.capacityWeight = capacityWeight;
        this.availability = availability;
        this.description = description;
        this.mKey = mKey;
        this.quantity = quantity;
        this.catMKey = catMKey;
        this.subCatMKey = subCatMKey;
        this.cartStatus = cartStatus;
        this.wishStatus = wishStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public String getCapacityWeight() {
        return capacityWeight;
    }

    public void setCapacityWeight(String capacityWeight) {
        this.capacityWeight = capacityWeight;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getmKey() {
        return mKey;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCatMKey() {
        return catMKey;
    }

    public void setCatMKey(String catMKey) {
        this.catMKey = catMKey;
    }

    public String getSubCatMKey() {
        return subCatMKey;
    }

    public void setSubCatMKey(String subCatMKey) {
        this.subCatMKey = subCatMKey;
    }

    public String getCartStatus() {
        return cartStatus;
    }

    public void setCartStatus(String cartStatus) {
        this.cartStatus = cartStatus;
    }

    public String getWishStatus() {
        return wishStatus;
    }

    public void setWishStatus(String wishStatus) {
        this.wishStatus = wishStatus;
    }
}
