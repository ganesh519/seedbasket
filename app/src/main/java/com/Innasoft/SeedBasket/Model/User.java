package com.Innasoft.SeedBasket.Model;

public class User {
    public String uId,username,mobile, email,password,role;

    public User(){

    }

    public User(String uId, String username, String mobile, String email, String password, String role) {
        this.uId = uId;
        this.username = username;
        this.mobile = mobile;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
