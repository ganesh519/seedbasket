package com.Innasoft.SeedBasket.Model;

public class AddAddressModel {
    String name,address1,address2,area,city,country,pincode,phone,alterno,check,state,key;

    public AddAddressModel(String name, String address1, String address2, String area, String city, String country, String pincode, String phone, String alterno, String check, String state, String key) {
        this.name = name;
        this.address1 = address1;
        this.address2 = address2;
        this.area = area;
        this.city = city;
        this.country = country;
        this.pincode = pincode;
        this.phone = phone;
        this.alterno = alterno;
        this.check = check;
        this.state = state;
        this.key = key;
    }

    public AddAddressModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAlterno() {
        return alterno;
    }

    public void setAlterno(String alterno) {
        this.alterno = alterno;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
