package com.Innasoft.SeedBasket.Model;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;

public class PostOrderModel {
    String userId;
    String addressId;
    String paymentId;
    String referenceId;
    String postKey;

    public PostOrderModel() {
    }

    public PostOrderModel(String userId, String addressId, String paymentId, String referenceId) {
        this.userId = userId;
        this.addressId = addressId;
        this.paymentId = paymentId;
        this.referenceId = referenceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    @Exclude
    public String getPostKey() {
        return postKey;
    }

    @Exclude
    public void setPostKey(String postKey) {
        this.postKey = postKey;
    }
}
