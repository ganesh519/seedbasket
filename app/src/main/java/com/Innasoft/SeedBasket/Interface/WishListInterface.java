package com.Innasoft.SeedBasket.Interface;

import com.Innasoft.SeedBasket.Model.Product;

public interface WishListInterface {

    public void onremoveFavorite(Product product);

}
