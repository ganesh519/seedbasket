package com.Innasoft.SeedBasket.Interface;



import com.Innasoft.SeedBasket.Model.PaymentModeModel;

import java.util.List;

public interface PaymentTypeInterface {

    void onItemClick(List<PaymentModeModel> paymentGatewayBeans, int i);
}
