package com.Innasoft.SeedBasket.Interface;

import com.Innasoft.SeedBasket.Model.Product;

public interface Cartinterface {

    void onMinusClick(Product product);

    void onPlusClick(Product product);

    void onRemoveDialog(Product product);

    void onWishListDialog(Product product);

}
