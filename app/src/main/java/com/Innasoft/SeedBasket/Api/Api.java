package com.Innasoft.SeedBasket.Api;

import com.Innasoft.SeedBasket.Model.LoginResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface Api {


    @GET
    Call<LoginResponse> userLogin(@Url String url);

}
