package com.Innasoft.SeedBasket.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.Innasoft.SeedBasket.Api.RetrofitClient;
import com.Innasoft.SeedBasket.Activities.LoginActivity;
import com.Innasoft.SeedBasket.Model.LoginResponse;
import com.Innasoft.SeedBasket.R;
import com.Innasoft.SeedBasket.Storage.PrefManagerUser;
import com.Innasoft.SeedBasket.Activities.HomeActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;

import am.appwise.components.ni.NoInternetDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static androidx.constraintlayout.widget.Constraints.TAG;


public class LoginFragment extends Fragment {
    Button btnLogin;
    TextView txtForgotPassword;
    TextView txtSignup;
    TextInputEditText etEmail,etPassword;
    private FirebaseAuth firebaseAuth;

    //progress dialog
    private ProgressDialog progressDialog;

    private NoInternetDialog noInternetDialog;
    PrefManagerUser session;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, null);
        txtSignup=view.findViewById(R.id.txtSignup);
        etEmail=view.findViewById(R.id.etEmail);
        etPassword=view.findViewById(R.id.etPassword);


        session=new PrefManagerUser(getActivity());

        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        String role = pref.getString("Role","");


        String next = "Not a member ? <font color='#f89113'>SignUp</font> now";
        txtSignup.setText(Html.fromHtml(next));


//        noInternetDialog = new NoInternetDialog.Builder(this).build();

        //getting firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();

        txtSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.putExtra("signup", "Sign");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);
            }
        });


        btnLogin=view.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userLogin();
            }
        });

        txtForgotPassword=view.findViewById(R.id.txtForgotPassword);
        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), ForgotPasswordActivity.class);
//                startActivity(intent);
            }
        });


        return view;
    }

    private void userLogin() {

        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();


        //checking if email and passwords are empty
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getActivity(), "Please enter email", Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getActivity(), "Please enter password", Toast.LENGTH_LONG).show();
            return;

        }

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Login Please Wait...");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(getActivity(), task -> {
                    progressDialog.dismiss();
                    //if the task is successfull
                    if (task.isSuccessful()) {

                        Log.e(TAG, "userLogin: " + task.getResult().getUser().getUid());
                        customLogin(email, password, task.getResult().getUser().getUid());

                    } else {
                        Toast.makeText(getActivity(), "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void customLogin(String email, String password, String uid) {

        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userLogin(RetrofitClient.BASE_URL + "UserData/" + uid + ".json");

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                progressDialog.dismiss();
                //  btnLogin.setEnabled(true);

                LoginResponse loginResponse = response.body();
                Log.d(TAG, "onResponse: " + loginResponse + "-----" + RetrofitClient.BASE_URL + "UserData/" + uid + ".json");
                if (loginResponse != null && loginResponse.getRole().equals("Users")) {

                    session.createLogin(loginResponse.getUId(),
                            loginResponse.getUsername(),
                            loginResponse.getEmail(),
                            loginResponse.getMobile(),
                            loginResponse.getRole());


                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } else {
                    int color = Color.RED;
                    // snackBar(loginResponse.getMessage(), color);
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                btnLogin.setEnabled(true);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
