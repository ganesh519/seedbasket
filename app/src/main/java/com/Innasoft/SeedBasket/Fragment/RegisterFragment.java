package com.Innasoft.SeedBasket.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.Innasoft.SeedBasket.Activities.LoginActivity;
import com.Innasoft.SeedBasket.Utilities.Constants;
import com.Innasoft.SeedBasket.Model.User;
import com.Innasoft.SeedBasket.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

import am.appwise.components.ni.NoInternetDialog;

import static android.content.Context.MODE_PRIVATE;


public class RegisterFragment extends Fragment {


    TextInputEditText etUsername;

    TextInputEditText etPhone;

    TextInputEditText etEmail;

    TextInputEditText etPassword;

    CheckBox checkbox;

    Button btnRegister;

    ProgressBar progressBar;

    TextView txtSignin;
    private NoInternetDialog noInternetDialog;
    private FirebaseAuth firebaseAuth;
    ProgressDialog progressDialog;
    String role;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, null);

        SharedPreferences pref = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        role = pref.getString("Role","");

        txtSignin=view.findViewById(R.id.txtSignin);
        etUsername=view.findViewById(R.id.etUsername);
        etPassword=view.findViewById(R.id.etPassword);
        checkbox=view.findViewById(R.id.checkbox);
        btnRegister=view.findViewById(R.id.btnRegister);
        progressBar=view.findViewById(R.id.progressBar);
        etPhone=view.findViewById(R.id.etPhone);
        etEmail=view.findViewById(R.id.etEmail);

        String next = "Already Registered ? <font color='#f89113'>Login</font> me";
        txtSignin.setText(Html.fromHtml(next));

        String checktext = "I agree to the Seed Basket ? <font color='#f89113'>Terms & conditions</font> and <font color='#f89113'>Privacy Policy</font>";
        checkbox.setText(Html.fromHtml(checktext));


        noInternetDialog = new NoInternetDialog.Builder(this).build();
        firebaseAuth = FirebaseAuth.getInstance();


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
        return view;
    }

    private void registerUser() {

        String userName = etUsername.getText().toString().trim();
        String mobile = etPhone.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();


        if (userName.isEmpty()) {
            etUsername.setError(getString(R.string.input_error_name));
            etUsername.requestFocus();
            return;
        }

        if (email.isEmpty()) {
            etEmail.setError(getString(R.string.input_error_email));
            etEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError(getString(R.string.input_error_email_invalid));
            etEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            etPassword.setError(getString(R.string.input_error_password));
            etPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            etPassword.setError(getString(R.string.input_error_password_length));
            etPassword.requestFocus();
            return;
        }

        if (mobile.isEmpty()) {
            etPhone.setError(getString(R.string.input_error_phone));
            etPhone.requestFocus();
            return;
        }

        if (mobile.length() != 10) {
            etPhone.setError(getString(R.string.input_error_phone_invalid));
            etPhone.requestFocus();
            return;
        }

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Registering Please Wait...");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                    //checking if success
                    if (task.isSuccessful()) {

                        User user = new User(
                                FirebaseAuth.getInstance().getCurrentUser().getUid(),
                                userName,
                                mobile,
                                email,
                                password,
                                role
                        );

                        FirebaseDatabase.getInstance().getReference(Constants.USERDATA_PATH)
                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .setValue(user).addOnCompleteListener(task1 -> {
                            progressDialog.dismiss();
                            if (task1.isSuccessful()) {

                                Toast.makeText(getActivity(), "registration success", Toast.LENGTH_LONG).show();
                                Intent intentAdmin = new Intent(getActivity(), LoginActivity.class);
                                intentAdmin.putExtra("ROLE",role);
                                startActivity(intentAdmin);

                            } else {
                                //display a failure message
                                Toast.makeText(getActivity(), task1.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });


                    } else {
                        progressDialog.dismiss();
                        //display some message here
                        Toast.makeText(getActivity(), "Registration Error", Toast.LENGTH_LONG).show();
                    }

                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }
}
